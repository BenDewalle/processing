import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__)))
##################
import obj.world as world
##################


class Launch(world.World):
    """
    Don't change anything here:
    everything is done in
    obj.world or other created modules
    """
    def __init__(self):
        super(Launch, self).__init__()
    ###############################

    def setup(self):
        # # settings
        size(self.width, self.height)
        background(self.bgCd)
        # # world
        self.setupWorld()


    def draw(self):
        # # world
        self.drawWorld()
