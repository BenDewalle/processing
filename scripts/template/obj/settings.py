import imp
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__)))
##################

class Settings(object):
    def __init__(self):
        """
        Set all the base settings that need to be shared between Classes
        """
        # # window
        self.width              = 1280
        self.height             = 720
        self.bgCd               = color(30, 20, 50)
