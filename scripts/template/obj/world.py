import imp
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__)))
##################
import obj.settings as settings
imp.reload(settings)
##################




class World(settings.Settings):
    """
    Just import objects and add them to world in populateWorld().
    Each imported class need a setup() or draw() function to be called
    """
    def __init__(self):
        # # settings
        super(World, self).__init__()
        # # create world array
        self.world = []
        self.populateWorld()

    ###############################
    def populateWorld(self):
        """
        append all Object to world
        """
        # # EXEMPLE:
        # rectangleObject = RectangleClass()
        # self.world.append(rectangleObject)
        pass

    ###############################
    def setupWorld(self):
        # launch all existing Object.setup() in self.world
        for obj in self.world:
            setupClass = getattr(obj, 'setup', None)
            if callable(setupClass):
                x = setupClass()

    def drawWorld(self):
        # launch all existing Object.draw() in self.world
        for obj in self.world:
            drawClass = getattr(obj, 'draw', None)
            if callable(drawClass):
                x = drawClass()

