import sys
import os
import imp
sys.path.append(os.path.join(os.path.dirname(__file__)))
import launch
imp.reload(launch)

def settings():
    global canva
    canva = launch.Launch()
    size(canva.width, canva.height)
def setup():
    canva.setup()
def draw():
    canva.draw()
