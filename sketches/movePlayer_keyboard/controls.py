def keyPressed():
    if key in ['z', 'Z']:
        p.u = 1
    if key in ['s', 'S']:
        p.d = 1
    if key in ['d', 'D']:
        p.r = 1
    if key in ['q', 'Q']:
        p.l = 1
    if keyCode == SHIFT:
        p.shiftPressed = 1
    if keyCode == UP:
        p.shiftPressed = 1

def keyReleased():
    if key in ['z', 'Z']:
        p.u = 0
    if key in ['s', 'S']:
        p.d = 0
    if key in ['d', 'D']:
        p.r = 0
    if key in ['q', 'Q']:
        p.l = 0
    if keyCode == SHIFT:
        p.shiftPressed = 0
    if keyCode == UP:
        p.shiftPressed = 0