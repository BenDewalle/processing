def setup():
    size(1000, 1000)
    global p
    p = Player ()

def draw():
    bgCd = 10
    background(bgCd)
    p.show()
    p.update()

class Player(object):
    def __init__(self):
        self.x = width*.5
        self.y = height*.5
        self.u = 0
        self.d = 0
        self.l = 0
        self.r = 0
        self.s = 8
        self.cd = 150

    def show(self):
        fill(self.cd)
        rect(self.x, self.y, 20, 20)

    def update(self):
        self.move()

    def move(self):
        self.x = self.x + ( self.r - self.l )*self.s
        self.y = self.y + ( self.d - self.u )*self.s

def keyPressed():
    if key in ['z', 'Z']:
        p.u = 1
    if key in ['s', 'S']:
        p.d = 1
    if key in ['d', 'D']:
        p.r = 1
    if key in ['q', 'Q']:
        p.l = 1
    

def keyReleased():
    if key in ['z', 'Z']:
        p.u = 0
    if key in ['s', 'S']:
        p.d = 0
    if key in ['d', 'D']:
        p.r = 0
    if key in ['q', 'Q']:
        p.l = 0