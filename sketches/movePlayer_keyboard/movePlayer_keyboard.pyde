def setup():
    size(1000, 1000)
    global p
    p = Player ()

def draw():
    bgCd = 10
    background(bgCd)
    p.show()
    p.update()

class Player(object):
    def __init__(self):
        self.x = width*.5
        self.y = height*.5
        self.u = 0
        self.d = 0
        self.l = 0
        self.r = 0
        self.s = 3
        self.cd = 150

    def show(self):
        fill(self.cd)
        rect(self.x, self.y, 20, 20)

    def update(self):
        self.x = self.x + ( self.r - self.l )*self.s
        self.y = self.y + ( self.d - self.u )*self.s

def keyPressed():
    if keyCode in ['z', 'Z']:
        p.u = 1
    if keyCode == 'S':
        p.d = 1
    if keyCode == 'Q':
        p.l = 1
    if keyCode == 'D':
        p.r = 1

def keyReleased():
    if keyCode == 'Z':
        p.u = 0
    if keyCode == 'S':
        p.d = 0
    if keyCode == 'Q':
        p.l = 0
    if keyCode == 'D':
        p.r = 0
