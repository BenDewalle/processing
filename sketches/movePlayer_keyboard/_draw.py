import sys
repoPth = 'D:/Boulot/processing/python/scripts/movePlayer_keyboard'
if repoPth not in sys.path:
    sys.path.append(repoPth)

import Canva
import controls
import Player
import Ground
#############

def setup():
    c = Canva()
    c.width
    c.height

    size(c.width, c.height)
    global p
    global g
    p = Player()
    g = Ground()

def draw():
    bgCd = 10
    background(bgCd)
    p.launch()
    g.launch()

