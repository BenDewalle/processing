class Canva(object):
    def __init__(self):
        self.width = 1000
        self.height = 1000
        self.bgCd = 30
        setBackground(self)

    def setBackground(self):
        background(self.bgCd)

    def set_width(self, width):
        self.width = width
    def get_width(self):
        return self.width

    def set_height(self, height):
        self.height = height
    def get_height(self):
        return self.height