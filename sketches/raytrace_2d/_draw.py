import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__)))
##################
import lib.world as world
##################


class Draw(world.World):
    """
    Don't change anything here:
    everything is done in
    lib.world or other created modules
    """
    def __init__(self):
        super(Draw, self).__init__()
    ###############################

    def setup(self):
        # # settings
        size(self.width, self.height)
        background(self.bgCd)
        # # world
        self.setupWorld()


    def draw(self):
        # # world
        self.drawWorld()
