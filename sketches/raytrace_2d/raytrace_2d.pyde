import sys
import os
import imp
sys.path.append(os.path.join(os.path.dirname(__file__)))
import _draw
imp.reload(_draw)

def settings():
    global canva
    canva = _draw.Draw()
    size(canva.width, canva.height)
def setup():
    canva.setup()
def draw():
    canva.draw()
