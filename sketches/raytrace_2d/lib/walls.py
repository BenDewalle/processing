import imp
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__)))
##################
import settings as settings
imp.reload(settings)
##################
import random


##################
class Wall(settings.Settings):
    def __init__(self):
        # # settings
        super(Wall, self).__init__()
        self.name = 'Wall'
        
        # # parameters
        self.x = 0
        self.y = 0
        self.xSpeed = 5


    def draw(self):
        # # color
        noFill()
        stroke(200)

        # # create
        pushMatrix()
        translate(self.x, self.y)
        line(0, 0, 0, 250)
        popMatrix()

        # # animate translate
        self.x += self.xSpeed
        if self.x >= self.width or self.x <= 0:
            self.xSpeed *= -1
