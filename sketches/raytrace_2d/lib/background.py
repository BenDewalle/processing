import imp
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__)))
##################
import settings as settings
imp.reload(settings)
##################
import random


##################
class BackGround(settings.Settings):
    def __init__(self):
        # # settings
        super(BackGround, self).__init__()
        
        # # parameters
        self.name = 'BackGround'
    
    def ColorRamp(self):
        noStroke()
        colorMode(HSB, 100)
        for i in range(100):
            for j in range(100):
                stroke(i, j, 100)
                point(i, j)

    def draw(self):
        background(self.bgCd)