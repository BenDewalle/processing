import imp
import sys
import os
import random
##################  BASE
sys.path.append(os.path.join(os.path.dirname(__file__)))
import settings
##################


##################
class Player(settings.Settings):
    def __init__(self):
        # # settings
        super(Player, self).__init__()
        
        # # parameters
        self.name = 'Player'
        self.rayCount = 6

    def setup(self):
        # # pos
        self.pos = PVector(self.width *.5, self.height *.25)
        self.dir = PVector(10, 0)
    
    def draw(self):
        # # color
        noFill()
        stroke(200)

        # # translate
        pushMatrix()
        translate(self.pos.x, self.pos.y)
        line(0, 0, self.dir.x, self.dir.y)
        popMatrix()

    def findIntersect(self, obstacle):
        """
        return PVector of intersecting pt between ray and obstacle or None
        """
        pass