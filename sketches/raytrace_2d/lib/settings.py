import imp
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__)))
##################

class Settings(object):
    def __init__(self):
        """
        Set all the base settings that need to be shared between Classes
        """
        # # window
        self.width              = 500
        self.height             = 500
        self.bgCd               = color(15, 20, 60)
