import imp
import sys
import os
##################  BASE
sys.path.append(os.path.join(os.path.dirname(__file__)))
import settings
##################  OBJECTS
import walls
import player
import background


class World(settings.Settings):
    """
    Just import objects and add them to world in populateWorld().
    Each imported class need a setup() or draw() function to be called
    """
    def __init__(self):
        # # settings
        super(World, self).__init__()
        # # create world array
        self.world = []
        self.populateWorld()


    ###############################
    def populateWorld(self):
        """
        append all Object to world
        """
        # # populate world
        bgObj = background.BackGround()
        self.world.append(bgObj)

        wallObj = walls.Wall()
        self.world.append(wallObj)

        playerObj = player.Player()
        self.world.append(playerObj)

        # # log
        for obj in self.world:
            objName = getattr(obj, 'name')
            print('World: populated ' + objName)

    ###############################
    def setupWorld(self):
        # launch all existing Object.setup() in self.world
        for obj in self.world:
            setupClass = getattr(obj, 'setup', None)
            if callable(setupClass):
                x = setupClass()
                # # log
                objName = getattr(obj, 'name')
                print('World: setup ' + objName)

    def drawWorld(self):
        # launch all existing Object.draw() in self.world
        for obj in self.world:
            drawClass = getattr(obj, 'draw', None)
            if callable(drawClass):
                x = drawClass()
                # # log
                if frameCount == 1:
                    objName = getattr(obj, 'name')
                    print('World: draw ' + objName)
